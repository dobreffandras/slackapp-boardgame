export class User {
    public readonly userId: string;
    public readonly canJoin: boolean;

    constructor(userSlackId: string, canJoin: boolean) {
        this.userId = userSlackId;
        this.canJoin = canJoin;
    }
}
