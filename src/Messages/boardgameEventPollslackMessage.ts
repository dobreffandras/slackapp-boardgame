import {BoardgameEvent} from "../boardgameEvent";
import {User} from "../user";

export class BoardGameEventPollSlackMessage {

    private static color: string = "#36a64f";
    private static HerokuColor: string = "#eeeeee";

    private static participantsToString(participants: User[]): string {
        return participants.length > 0 ? participants.map((p) => "<@" + p.userId + ">").join(", ") : "No one";
    }

    private event: BoardgameEvent;
    private dbId: number;

    private users: User[];

    constructor(event: BoardgameEvent, dbId: number, participants?: User[]) {
        this.event = event;
        this.dbId = dbId;
        this.users = participants;
    }

    public toJSON(): object {
        return {
            response_type: "in_channel",
            text: "A new Boardgame event!",
            attachments: [{
                fallback: "The description of the game",
                color: BoardGameEventPollSlackMessage.color,
                title: this.event.game.Name,
                title_link: "https://boardgamegeek.com/boardgame/" + this.event.game.id,
                thumb_url: this.event.game.ThumbnailURL,
                fields: [{
                    title: "Players",
                    value: this.getPlayersCount(),
                    short: true,
                }, {
                    title: "Playing time",
                    value: this.getPlayingTime(),
                    short: true,
                }, {
                    title: "Date",
                    value: this.event.date.format("MMM DD"),
                    short: true,
                }, {
                    title: "Time",
                    value: this.event.date.format("HH:mm"),
                    short: true,
                }],
            }, {
                fallback: "The users",
                color: BoardGameEventPollSlackMessage.color,
                attachment_type: "default",
                fields: [{
                    title: "Participants:",
                    value: this.getParticipants(),
                    short: false,
                }, {
                    title: "Cannot make it:",
                    value: this.getRebels(),
                    short: false,
                }],
            }, {
                text: "Do you participate?",
                fallback: "You are unable to choose a game",
                callback_id: this.dbId,
                color: BoardGameEventPollSlackMessage.color,
                attachment_type: "default",
                actions: [
                    {
                        name: "game",
                        text: "Let's do it!",
                        type: "button",
                        style: "primary",
                        value: "yes",
                    },
                    {
                        name: "game",
                        text: "No, I'm unsocial...",
                        type: "button",
                        style: "default",
                        value: "no",
                    },
                ],
            }, {
                fallback: "Heroku information message",
                color: BoardGameEventPollSlackMessage.HerokuColor,
                attachment_type: "default",
                text: "This app is hosted on a free Heroku Plan. Sometimes the service is unavailable. In these cases please try again your action some minutes later!",
            }],
        };
    }

    private getPlayingTime() {
        let playingTime;
        if (this.event.game.MinPlayingTime === this.event.game.MaxPlayingTime) {
            playingTime = this.event.game.MinPlayingTime;
        } else {
            playingTime = this.event.game.MinPlayingTime + " - " + this.event.game.MaxPlayingTime;
        }
        return playingTime + " min.";
    }

    private getPlayersCount() {
        let players;
        if (this.event.game.MinPlayers === this.event.game.MaxPlayers) {
            players = this.event.game.MinPlayers;
        } else {
            players = this.event.game.MinPlayers + " - " + this.event.game.MaxPlayers;
        }
        return players;
    }

    private getRebels(): string {
        const rebels = this.users ? this.users.filter((p) => !p.canJoin) : [];
        return BoardGameEventPollSlackMessage.participantsToString(rebels);
    }

    private getParticipants(): string {
        const participants = this.users ? this.users.filter((p) => p.canJoin) : [];
        return BoardGameEventPollSlackMessage.participantsToString(participants);
    }
}
