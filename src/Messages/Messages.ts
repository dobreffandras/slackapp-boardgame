export interface ITextMessage {
    text: string;
    response_type: string;
    replace_original: boolean;
}

export class Messages {

    public static readonly SuccessfulParticipationMessage: ITextMessage =
        Messages.getEphemeralMessage("Awesome! Thank you joining the game! :+1: :100:");

    public static readonly UnSuccessfulParticipationMessage: ITextMessage =
        Messages.getEphemeralMessage("We appreciate your response. It's sad, that you cannot join :disappointed_relieved:");

    public static WrongFormatMessage: ITextMessage =
        Messages.getEphemeralMessage("Oops... You used a wrong format. You can use `/boardgame help` for more information.");

    public static readonly getHelpMessage = (hostName: string, protocol: string): ITextMessage =>
        Messages.getEphemeralMessage("With the /boardgame slash command you are able to organize boardgame events. \n\n" +
            "Please visit http://boardgamegeek.com and select the game you'd like to play. You can retrieve the BoardGameGeek Id from the URL. \n\n" +
            "You have to give the BoardGameGeek Id, the date, and the time with the /boardgame slash command in the following format: \n" +
            "`/boardgame <BGGID> <date> <time>` \n" +
            "Example:\n" +
            "`/boardgame 13 2017-04-29 17:00` \n\n" +
            "For more information please visit " + protocol + "://" + hostName)

    public static getEphemeralMessage(message: string): ITextMessage {
        return {
            text: message,
            response_type: "ephemeral",
            replace_original: false,
        };
    }
}
