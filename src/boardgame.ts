export class BoardGame {
    public id: number;
    public Name: string;
    public MinPlayers: number;
    public MaxPlayers: number;
    public MinPlayingTime: number;
    public MaxPlayingTime: number;
    public ThumbnailURL: string;
}
