import * as moment from "moment";
import {BoardGame} from "./boardgame";

export class BoardgameEvent {
    public game: BoardGame;
    public date: moment.Moment;

    constructor(game: BoardGame, date: moment.Moment) {
        this.game = game;
        this.date = date;
    }
}
