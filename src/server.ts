import * as bodyParser from "body-parser";
import * as express from "express";
import * as path from "path";
import {CommandRoute} from "./routes/command";
import {IndexRoute} from "./routes/index";
import {ReactRoute} from "./routes/react";

/**
 * The server.
 *
 * @class Server
 */
export class Server {

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    public app: express.Application;

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        this.app = express();

        this.config();

        this.routes();

        this.api();
    }

    /**
     * Create REST API routes
     *
     * @class Server
     * @method api
     */
    public api() {
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {
        this.app.use(bodyParser.urlencoded({
            extended: true,
        }));
        // catch 404 and forward to error handler
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            err.status = 404;
            next(err);
        });
    }

    /**
     * Create router.
     *
     * @class Server
     * @method config
     * @return void
     */
    private routes() {
        let router: express.Router;
        router = express.Router();

        // IndexRoute
        IndexRoute.create(router);
        CommandRoute.create(router);
        ReactRoute.create(router);

        // use router middleware
        this.app.use(router);

        // use static middleware
        this.app.use(express.static(path.join(__dirname, "..", "public")));
    }
}
