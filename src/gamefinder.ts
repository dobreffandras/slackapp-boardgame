import * as request from "request";
import * as parser from "xml2json";
import {BoardGame} from "./boardgame";

export class GameFinder {

    public static readonly GameDoesNotExistErrorMessage: string = "Ooops... The game with the given BGGID does not exist!";

    public static find(BGGID: number): Promise<BoardGame> {
        return new Promise(async (resolve, reject) => {
            try {
                const gameResponse = await GameFinder.call(BGGID);
                if (!gameResponse.error) {
                    const name = gameResponse.name instanceof Array ? gameResponse.name.find((e) => e.primary === "true").$t : gameResponse.name.$t;

                    resolve({
                        ...new BoardGame(),
                        id: parseInt(gameResponse.objectid),
                        Name: name,
                        MinPlayers: parseInt(gameResponse.minplayers),
                        MaxPlayers: parseInt(gameResponse.maxplayers),
                        MinPlayingTime: parseInt(gameResponse.minplaytime),
                        MaxPlayingTime: parseInt(gameResponse.maxplaytime),
                        ThumbnailURL: gameResponse.thumbnail,
                    });
                } else {
                    reject(gameResponse.error.message);
                }
            } catch (err) {
                reject(err);
            }
        });
    }

    private static async call(BGGID: number): Promise<any> {
        return new Promise((resolve, reject) => request({
            url: "https://www.boardgamegeek.com/xmlapi/boardgame/" + BGGID,
            method: "GET",
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                reject(error);
            } else {
                const jsonBody = JSON.parse(parser.toJson(body));
                resolve(jsonBody.boardgames.boardgame);
            }
        }).end());
    }
}
