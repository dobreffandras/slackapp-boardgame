import {Query, QueryConfig, QueryResult} from "pg";

export interface IClient {
    connect(): Promise<void>;

    connect(callback: (err: Error) => void): void;

    end(): Promise<void>;

    end(callback: (err: Error) => void): void;

    query(queryTextOrConfig: string | QueryConfig, callback: (err: Error, result: QueryResult) => void): Query;
}
