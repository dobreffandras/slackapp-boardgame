import {Client, Query, QueryConfig, QueryResult} from "pg";
import {IClient} from "./iclient";

export class BoargameDBClient implements IClient {

    private client: Client;

    constructor() {
        this.client = new Client({
            connectionString: process.env.DATABASE_URL,
            ssl: process.env.SSL !== "false",
        });
    }

    public connect(): Promise<void>;
    public connect(callback: (err: Error) => void): void;
    public connect(callback?: (err: Error) => void): Promise<void> | void {
        if (callback) {
            this.client.connect(callback);
        } else {
            return this.client.connect();
        }
    }

    public end(): Promise<void>;
    public end(callback: (err: Error) => void): void;
    public end(callback?: (err: Error) => void): Promise<void> | void {
        if (callback) {
            this.client.end(callback);
        } else {
            return this.client.end();
        }
    }

    public query(queryTextOrConfig: string | QueryConfig, callback: (err: Error, result: QueryResult) => void): Query {
        return this.client.query(queryTextOrConfig, callback);
    }
}
