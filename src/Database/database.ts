import * as moment from "moment";
import {BoardgameEvent} from "../boardgameEvent";
import {GameFinder} from "../gamefinder";
import {User} from "../user";
import {IClient} from "./iclient";

export class Database {

    public static readonly DBInsertErrorMessage: string = "Ooops, something went wrong when saving the data...";

    private client: IClient;

    constructor(boargameDBClient: IClient) {
        this.client = boargameDBClient;
    }

    public saveEvent(event: BoardgameEvent): Promise<number> {
        return new Promise((resolve, reject) => {
            const queryStr = "INSERT INTO boardgameevents (bggid, eventdate) VALUES('" + event.game.id + "' , to_timestamp(" + event.date.unix() + "))  RETURNING id;";
            this.client.query(queryStr, (queryError, queryResult) => {
                if (queryError) {
                    reject(queryError);
                } else {
                    resolve(queryResult.rows[0].id);
                }
            });
        });
    }

    public getEvent(eventID: number): Promise<BoardgameEvent> {
        return new Promise((resolve, reject) => {
            const queryStr = "SELECT bggid, eventdate FROM boardgameevents WHERE id=" + eventID + ";";
            this.client.query(queryStr, (queryError, queryResult) => {
                if (queryError) {
                    reject(queryError);
                } else {
                    if (queryResult.rows && queryResult.rows[0]) {
                        const queryRow = queryResult.rows[0];
                        GameFinder.find(queryRow.bggid).then((game) => {
                            const event = new BoardgameEvent(game, moment(queryRow.eventdate));
                            resolve(event);
                        }).catch((reason) => {
                            reject(reason);
                        });
                    } else {
                        reject("There is no such event");
                    }
                }
            });
        });
    }

    public registerUserToEvent(teamID: string, userID: string, eventID: number, canJoin: boolean): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const queryStr =
                "UPDATE participations SET canjoin='" + canJoin + "' WHERE  event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'; " +
                "INSERT INTO participations (event_id, user_slack_id, team_slack_id, canjoin) SELECT " + eventID + " , '" + userID + "','" + teamID + "', '" + canJoin + "' " +
                "WHERE NOT EXISTS (SELECT 1 FROM participations WHERE event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'); ";

            this.client.query(queryStr, (queryError, queryResult) => {
                if (queryError) {
                    reject(queryError);
                } else {
                    resolve();
                }
            });
        });
    }

    public getParticipantsForEvent(teamID: string, eventDBID: number): Promise<User[]> {
        return new Promise((resolve, reject) => {
            const queryStr = "SELECT user_slack_id, canjoin FROM participations WHERE event_id = " + eventDBID + " AND team_slack_id = '" + teamID + "';";
            this.client.query(queryStr, (queryError, queryResult) => {
                if (queryError) {
                    reject(queryError);
                } else {
                    const participations = queryResult.rows.map((row) => new User(row.user_slack_id, row.canjoin));
                    resolve(participations);
                }
            });
        });
    }

    public connect(): Promise<void> {
        return this.client.connect();
    }

    public disconnect(): Promise<void> {
        return this.client.end();
    }
}
