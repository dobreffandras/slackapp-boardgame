import {BoargameDBClient} from "./BoargameDBClient";
import {Database} from "./database";

export class DbFactory {
    public static createDatabase(): Database {
        return new Database(new BoargameDBClient());
    }
}
