import * as moment from "moment";

export class DateParser {
    public static parse(dateString: string, timestring): moment.Moment {
        const dateTimeString = dateString + " " + timestring;
        return moment(dateTimeString, [moment.ISO_8601, "YYYY-MM-DD HH:mm"]);
    }
}
