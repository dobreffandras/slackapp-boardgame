export class AppVerifier {
    public static async verify(appVerifyToken: string, success: () => void, failure: () => void) {
        if (appVerifyToken === process.env.VERIFY_TOKEN) {
            await success();
        } else {
            await failure();
        }
    }
}
