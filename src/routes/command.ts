import {NextFunction, Request, Response, Router} from "express";
import {AppVerifier} from "../AppVerifier";
import {BoardgameEvent} from "../boardgameEvent";
import {Database} from "../Database/database";
import {DbFactory} from "../Database/dbfactory";
import {DateParser} from "../dateparser";
import {GameFinder} from "../gamefinder";
import {BoardGameEventPollSlackMessage} from "../Messages/boardgameEventPollslackMessage";
import {Messages} from "../Messages/Messages";
import {BaseRoute} from "./route";

/**
 * / route
 *
 * @class User
 */
export class CommandRoute extends BaseRoute {

    /**
     * Create the routes.
     *
     * @class CommandRoute
     * @method create
     * @static
     */
    public static create(router: Router) {
        console.log("[CommandRoute::create] Creating command route.");

        router.post("/command", (req: Request, res: Response, next: NextFunction) => {
            new CommandRoute(DbFactory.createDatabase()).command(req, res, next);
        });
    }

    private database: Database;

    /**
     * Constructor
     *
     * @class CommandRoute
     * @constructor
     */
    constructor(database: Database) {
        super();
        this.database = database;
    }

    /**
     * The home page route.
     *
     * @class CommandRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
     */
    public async command(req: Request, res: Response, next: NextFunction) {
        await AppVerifier.verify(req.body.token, async () => {
            const text = req.body.text;
            const args = text ? text.split(" ") : [];

            if (args.length === 3) {
                const BGGID = parseInt(args[0]);
                const date = args[1];
                const time = args[2];

                try {
                    const game = await GameFinder.find(BGGID);
                    try {
                        const event = new BoardgameEvent(game, DateParser.parse(date, time));
                        await this.database.connect();
                        const insertedId = await this.database.saveEvent(event);
                        const poll = new BoardGameEventPollSlackMessage(event, insertedId);
                        res.setHeader("Content-Type", "application/json");
                        BaseRoute.sendJSON(req, res, poll.toJSON());
                    } catch (err) {
                        console.log(err);
                        BaseRoute.sendJSON(req, res, Messages.getEphemeralMessage(Database.DBInsertErrorMessage));
                    } finally {
                        await this.database.disconnect();
                    }
                } catch (err) {
                    console.log(err);
                    BaseRoute.sendJSON(req, res, Messages.getEphemeralMessage(GameFinder.GameDoesNotExistErrorMessage));
                }
            } else if (text === "help") {
                BaseRoute.sendJSON(req, res, Messages.getHelpMessage(req.hostname, req.protocol));
            } else {
                BaseRoute.sendJSON(req, res, Messages.WrongFormatMessage);
            }
        }, () => {
            const message = "The application rejected the request. Request was not verified";
            console.log(message);
            res.send(message);
        });
    }
}
