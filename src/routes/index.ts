import {NextFunction, Request, Response, Router} from "express";
import * as path from "path";
import {BaseRoute} from "./route";

/**
 * / route
 *
 * @class User
 */
export class IndexRoute extends BaseRoute {

    /**
     * Create the routes.
     *
     * @class IndexRoute
     * @method create
     * @static
     */
    public static create(router: Router) {
        console.log("[IndexRoute::create] Creating index route.");

        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().index(req, res, next);
        });
    }

    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
     */
    public index(req: Request, res: Response, next: NextFunction) {
        const options: object = {
            message: "Welcome to the Boardgame Slack app",
            root: path.join(__dirname, "../../public"),
        };

        this.sendFile(req, res, "index.html", options);
    }
}
