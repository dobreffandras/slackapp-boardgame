import {NextFunction, Request, Response, Router} from "express";
import {AppVerifier} from "../AppVerifier";
import {Database} from "../Database/database";
import {DbFactory} from "../Database/dbfactory";
import {BoardGameEventPollSlackMessage} from "../Messages/boardgameEventPollslackMessage";
import {Messages} from "../Messages/Messages";
import {BaseRoute} from "./route";

/**
 * / route
 *
 * @class User
 */
export class ReactRoute extends BaseRoute {

    /**
     * Create the routes.
     *
     * @class ReactRoute
     * @method create
     * @static
     */
    public static create(router: Router) {
        console.log("[ReactRoute::create] Creating react route.");

        router.post("/react", (req: Request, res: Response, next: NextFunction) => {
            new ReactRoute(DbFactory.createDatabase()).react(req, res, next);
        });
    }

    private database: Database;

    /**
     * Constructor
     *
     * @class ReactRoute
     * @constructor
     */
    constructor(database: Database) {
        super();
        this.database = database;
    }

    /**
     * The home page route.
     *
     * @class ReactRoute
     * @method react
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
     */
    public async react(req: Request, res: Response, next: NextFunction) {
        const payload = JSON.parse(req.body.payload);
        await AppVerifier.verify(payload.token, async () => {
            const participationResponse = payload.actions[0];
            const responseURL = payload.response_url;
            const eventDBID = parseInt(payload.callback_id);

            if (participationResponse.name === "game") {
                const userVotedYes = participationResponse.value === "yes";
                try {
                    await this.database.connect();
                    await this.database.registerUserToEvent(payload.team.id, payload.user.id, eventDBID, userVotedYes);
                    const event = await this.database.getEvent(eventDBID);
                    const participants = await this.database.getParticipantsForEvent(payload.team.id, eventDBID);
                    const responseMessage = userVotedYes ? Messages.SuccessfulParticipationMessage : Messages.UnSuccessfulParticipationMessage;
                    const eventMessage = new BoardGameEventPollSlackMessage(event, eventDBID, participants);
                    BaseRoute.sendInteractiveResponse(req, res, eventMessage.toJSON(), responseMessage, responseURL);
                } catch (error) {
                    console.log(error);
                    BaseRoute.sendJSON(req, res, Messages.getEphemeralMessage(Database.DBInsertErrorMessage));
                } finally {
                    await this.database.disconnect();
                }
            } else {
                console.log("Invalid Push Button");
            }
        }, () => {
            const message = "The application rejected the request. Request was not verified";
            console.log(message);
            res.send(message);
        });
    }
}
