import {NextFunction, Request, Response} from "express";
import * as https from "https";
import * as url from "url";
import {BoardGameEventPollSlackMessage} from "../Messages/boardgameEventPollslackMessage";
import {ITextMessage} from "../Messages/Messages";

/**
 * Constructor
 *
 * @class BaseRoute
 */
export class BaseRoute {

    /**
     * Sends a plain text response.
     *
     * @class BaseRoute
     * @method sendFile
     * @param req {Request} The request object.
     * @param res {Response} The response object.
     * @param message
     * @return void
     */
    public static send(req: Request, res: Response, message: string) {
        res.locals.BASE_URL = "/";
        res.send(message);
    }

    /**
     * Sends a plain text response.
     *
     * @class BaseRoute
     * @method sendFile
     * @param req {Request} The request object.
     * @param res {Response} The response object.
     * @param message
     * @return void
     */
    public static sendJSON(req: Request, res: Response, message: object) {
        res.locals.BASE_URL = "/";
        res.send(message);
    }

    /**
     * Sends a plain text response.
     *
     * @class BaseRoute
     * @method sendFile
     * @param req {Request} The request object.
     * @param res {Response} The response object.
     * @param publicMessage
     * @param ephemeralMessage
     * @param responseURLString
     * @return void
     */
    public static sendInteractiveResponse(req: Request,
                                          res: Response,
                                          publicMessage: any,
                                          ephemeralMessage: ITextMessage,
                                          responseURLString: string) {
        // send ephemeral
        res.locals.BASE_URL = "/";
        res.send(ephemeralMessage);

        // send public
        publicMessage.replace_orignal = true;
        this.sendPostRequest(responseURLString, publicMessage);
    }

    private static sendPostRequest(requestURLString: string, message: any) {
        const requestURL = url.parse(requestURLString);

        const postData = JSON.stringify(message);
        const postReq = https.request({
            host: requestURL.host,
            port: requestURL.port,
            method: "POST",
            path: requestURL.path,
            headers: {
                "Content-Type": "application/json",
                "Content-Length": Buffer.byteLength(postData),
            },
        }, ((res) => {
            res.setEncoding("utf8");
            res.on("data", (chunk) => {
                console.log("Response: " + chunk);
            });
        }));

        postReq.on("error", (e) => {
            console.error(e);
        });
        postReq.write(postData);
        postReq.end();
    }

    protected title: string;

    private scripts: string[];

    /**
     * Constructor
     *
     * @class BaseRoute
     * @constructor
     */
    constructor() {
        this.title = "";
        this.scripts = [];
    }
    /**
     * Add a JS external file to the request.
     *
     * @class BaseRoute
     * @method addScript
     * @param src {string} The src to the external JS file.
     * @return {BaseRoute} Self for chaining
     */
    public addScript(src: string): BaseRoute {
        this.scripts.push(src);
        return this;
    }

    /**
     * Render a page.
     *
     * @class BaseRoute
     * @method sendFile
     * @param req {Request} The request object.
     * @param res {Response} The response object.
     * @param view {String} The view to sendFile.
     * @param options {Object} Additional options to append to the view's local scope.
     * @return void
     */
    public sendFile(req: Request, res: Response, view: string, options?: object) {
        res.locals.BASE_URL = "/";

        res.locals.scripts = this.scripts;

        res.locals.title = this.title;

        res.sendFile(view, options);
    }
}
