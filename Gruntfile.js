module.exports = function (grunt) {
    "use strict";

    grunt.initConfig({
        clean: ["./dist"],
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: "./public",
                        src: ["**"],
                        dest: "./dist/public"
                    },
                    {
                        expand: true,
                        cwd: "./views",
                        src: ["**"],
                        dest: "./dist/views"
                    }
                ]
            }
        },
        ts: {
            dev: {
                files: [{
                    src: ["tests/\*\*/\*.ts", "!tests/.baseDir.ts"],
                    dest: "./dist"
                }, {
                    src: ["src/\*\*/\*.ts", "!src/.baseDir.ts"],
                    dest: "./dist"
                }],
                options: {
                    module: "commonjs",
                    target: "es6",
                    sourceMap: false,
                    rootDir: "."
                }
            },
            app: {
                files: [{
                    src: ["src/\*\*/\*.ts", "!src/.baseDir.ts"],
                    dest: "./dist"
                }],
                options: {
                    module: "commonjs",
                    target: "es6",
                    sourceMap: false,
                    rootDir: "."
                }
            }
        },
        tslint: {
            options: {
                configuration: "tslint.json",
                force: false,
                fix: false
            },
            dev: {
              files:{
                src: ["src/**/*.ts"]
              }
            }
          },
        env:{
            dev:{
              src: "devenv.json"
            }
        },
        run: {
            options: {},
            target: {
              cmd: 'node',
              args: [
                './bin/www.js'
              ]
            }
        },
        watch: {
            ts: {
                files: ["src/\*\*/\*.ts"],
                tasks: ["ts"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-run');
    grunt.loadNpmTasks('grunt-tslint');

    grunt.registerTask("build", [
        "copy",
        "ts:app"
    ]);

    grunt.registerTask("build-clean", [
        "clean",
        "build"
    ]);

    grunt.registerTask("run-dev", [
        "env:dev",
        "run:target"
    ]);

    grunt.registerTask("build-dev", [
        "clean",
        "copy",
        "tslint:dev",
        "ts:dev"
    ]);

    grunt.registerTask("default", [ "build" ]);
};