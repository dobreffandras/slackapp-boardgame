import {DateParser} from "../src/dateparser";
import moment = require("moment");

describe("DateParser Unit Tests", () => {
    it('should parse a ISO-8601 date and time', () => {
        // given
        const dateString = "2017-11-04";
        const timeString = "17:00";

        // when
        const result = DateParser.parse(dateString, timeString);

        // then
        expect(result.unix()).toEqual(moment("2017-11-04 17:00").unix());
    });

    it('should parse a date with non-leading zeros for months', () => {
        // given
        const dateString = "2017-8-04";
        const timeString = "17:00";

        // when
        const result = DateParser.parse(dateString, timeString);

        // then
        expect(result.unix()).toEqual(moment("2017-08-04 17:00").unix());
    });

    it('should parse a date with non-leading zeros for days', () => {
        // given
        const dateString = "2017-11-4";
        const timeString = "17:00";

        // when
        const result = DateParser.parse(dateString, timeString);

        // then
        expect(result.unix()).toEqual(moment("2017-11-04 17:00").unix());
    });

    it('should parse a date with non-leading zeros for hours', () => {
        // given
        const dateString = "2017-11-04";
        const timeString = "7:00";

        // when
        const result = DateParser.parse(dateString, timeString);

        // then
        expect(result.unix()).toEqual(moment("2017-11-04 07:00").unix());
    });

    it('should parse a date with year short notation', () => {
        // given
        const dateString1 = "89-11-04";
        const dateString2 = "17-11-04";
        const dateString3 = "'17-11-04";
        const timeString = "17:00";

        // when
        const result1 = DateParser.parse(dateString1, timeString);
        const result2 = DateParser.parse(dateString2, timeString);
        const result3 = DateParser.parse(dateString3, timeString);

        // then
        expect(result1.unix()).toEqual(moment("1989-11-04 17:00").unix());
        expect(result2.unix()).toEqual(moment("2017-11-04 17:00").unix());
        expect(result3.unix()).toEqual(moment("2017-11-04 17:00").unix());
    });

    it('should parse a date with several simplifications', () => {
        // given
        const dateString = "89-7-4";
        const timeString = "7:00";

        // when
        const result1 = DateParser.parse(dateString, timeString);
        // then
        expect(result1.unix()).toEqual(moment("1989-07-04 07:00").unix());
    })
});



