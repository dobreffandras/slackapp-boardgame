import * as moment from "moment";
import * as TypeMoq from "typemoq";
import {BoardGame} from "../../src/boardgame";
import {BoardgameEvent} from "../../src/boardgameEvent";
import {Database} from "../../src/Database/database";
import {IClient} from "../../src/Database/iclient";
import {GameFinder} from "../../src/gamefinder";
import {User} from "../../src/user";
const pgtest = require("pgtest");

describe("Database Unit Tests", () => {

    it("saveEvent test", () => {
        // given
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const instance: Database = new Database(client.object);
        const boardgame: BoardGame = new BoardGame();
        boardgame.id = 42;
        const eventDate: moment.Moment = moment(0);

        // when
        instance.connect();
        instance.saveEvent(new BoardgameEvent(boardgame, eventDate));
        instance.disconnect();

        // then
        const expectedSQLQueryString = "INSERT INTO boardgameevents (bggid, eventdate) VALUES('" + boardgame.id + "' , to_timestamp(" + eventDate.unix() + "))  RETURNING id;";
        client.verify((c) => c.connect(), TypeMoq.Times.once());
        client.verify((c) => c.query(TypeMoq.It.isValue(expectedSQLQueryString), TypeMoq.It.isAny()), TypeMoq.Times.once());
        client.verify((c) => c.end(), TypeMoq.Times.once());
    });

    it("getEvent test", () => {
        // given
        const eventID = 42;
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const instance: Database = new Database(client.object);

        // when
        instance.connect();
        instance.getEvent(eventID);
        instance.disconnect();

        // then
        const expectedSQLQueryString = "SELECT bggid, eventdate FROM boardgameevents WHERE id=" + eventID + ";";
        client.verify((c) => c.connect(), TypeMoq.Times.once());
        client.verify((c) => c.query(TypeMoq.It.isValue(expectedSQLQueryString), TypeMoq.It.isAny()), TypeMoq.Times.once());
        client.verify((c) => c.end(), TypeMoq.Times.once());
    });

    it("registerUserToEvent test", () => {
        // given
        const userID = "U7664";
        const teamID = "T43524";
        const canJoin = true;
        const eventID = 42;
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const instance: Database = new Database(client.object);

        // when
        instance.connect();
        instance.registerUserToEvent(teamID, userID, eventID, canJoin);
        instance.disconnect();

        // then
        const expectedSQLQueryString =
            "UPDATE participations SET canjoin='" + canJoin + "' WHERE  event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'; " +
            "INSERT INTO participations (event_id, user_slack_id, team_slack_id, canjoin) SELECT " + eventID + " , '" + userID + "','" + teamID + "', '" + canJoin + "' " +
            "WHERE NOT EXISTS (SELECT 1 FROM participations WHERE event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'); ";
        client.verify((c) => c.connect(), TypeMoq.Times.once());
        client.verify((c) => c.query(TypeMoq.It.isValue(expectedSQLQueryString), TypeMoq.It.isAny()), TypeMoq.Times.once());
        client.verify((c) => c.end(), TypeMoq.Times.once());
    });

    it("getParticipantsForEvent test", () => {
        // given
        const teamID = "T3465";
        const eventID = 42;
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const instance: Database = new Database(client.object);

        // when
        instance.connect();
        instance.getParticipantsForEvent(teamID, eventID);
        instance.disconnect();

        // then
        const expectedSQLQueryString = "SELECT user_slack_id, canjoin FROM participations WHERE event_id = " + eventID + " AND team_slack_id = '" + teamID + "';";
        client.verify((c) => c.connect(), TypeMoq.Times.once());
        client.verify((c) => c.query(TypeMoq.It.isValue(expectedSQLQueryString), TypeMoq.It.isAny()), TypeMoq.Times.once());
        client.verify((c) => c.end(), TypeMoq.Times.once());
    });

    describe("saveEvent tests with pgtest", () => {

        beforeEach(() => {
            pgtest.reset();
        });

        it("saveEvent should return the appropriate board game", async (endTest) => {
            // given
            const BGGID = 30549;
            const boardgame = {...new BoardGame(), id: BGGID};
            const eventDate = moment(1509573661);
            const event = new BoardgameEvent(boardgame, eventDate);
            const expectedID = 1;
            let resultId: Promise<number>;

            pgtest.expect("INSERT INTO boardgameevents (bggid, eventdate) VALUES('" + boardgame.id + "' , to_timestamp(" + eventDate.unix() + "))  RETURNING id;").returning(null, [
                {id: expectedID},
            ]);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };
                instance.connect();
                resultId = instance.saveEvent(event);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                const id = await resultId;
                expect(id).toEqual(expectedID);
            } finally {
                endTest();
            }
        });

        it("saveEvent should reject if DB error occurs", async (endTest) => {
            // given
            const BGGID = 30549;
            const boardgame = {...new BoardGame(), id: BGGID};
            const eventDate = moment(1509573661);
            const event = new BoardgameEvent(boardgame, eventDate);
            let resultId: Promise<number>;
            const expectedErrorMessage = "Some DB Error";

            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };
                // when
                instance.connect();
                resultId = instance.saveEvent(event);
                instance.disconnect();

                done();
            });

            pgtest.expect("INSERT INTO boardgameevents (bggid, eventdate) VALUES('" + boardgame.id + "' , to_timestamp(" + eventDate.unix() + "))  RETURNING id;").returning(
                new Error(expectedErrorMessage), null);

            // then
            try {
                pgtest.check();
                await resultId;
            } catch (err) {
                expect(err.message).toEqual(expectedErrorMessage);
            } finally {
                endTest();
            }
        });
    });

    describe("getEvent tests with pgtest", () => {

        beforeEach(() => {
            pgtest.reset();
        });

        it("getEvent should return the appropriate board game", async (endTest) => {
            // given
            const eventID = 42;
            const BGGID = 30549;

            let event: Promise<BoardgameEvent>;
            spyOn(GameFinder, "find").and.returnValue(Promise.resolve({...new BoardGame(), id: BGGID}));

            pgtest.expect("SELECT bggid, eventdate FROM boardgameevents WHERE id=" + eventID + ";").returning(null, [
                {bggid: BGGID, eventdate: 1509573661},
            ]);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                event = instance.getEvent(eventID);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                const e = await event;
                expect(e.game.id).toEqual(BGGID);
            } finally {
                endTest();
            }
        });
        it("getEvent should reject if the game cannot be found", async (endTest) => {
            // given
            const eventID = 42;
            const BGGID = 30549;

            let event: Promise<BoardgameEvent>;
            const promiseRejectionReason = "Game not found";
            spyOn(GameFinder, "find").and.returnValue(Promise.reject(promiseRejectionReason));

            pgtest.expect("SELECT bggid, eventdate FROM boardgameevents WHERE id=" + eventID + ";").returning(null, [
                {bggid: BGGID, eventdate: 1509573661},
            ]);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                event = instance.getEvent(eventID);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                await event;
            } catch (err) {
                expect(err).toEqual(promiseRejectionReason);
            } finally {
                endTest();
            }
        });

        it("getEvent should reject if there is no event with the id", async (endTest) => {
            // given
            const eventID = 42;
            let event: Promise<BoardgameEvent>;
            const promiseRejectionReason = "There is no such event";
            spyOn(GameFinder, "find"); // Prevent calling real API

            pgtest.expect("SELECT bggid, eventdate FROM boardgameevents WHERE id=" + eventID + ";").returning(null, []);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };
                // when
                instance.connect();
                event = instance.getEvent(eventID);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                await event;
            } catch (err) {
                expect(err).toEqual(promiseRejectionReason);
            } finally {
                endTest();
            }
        });
        it("getEvent should reject if DB error occurs", async (endTest) => {
            // given
            const eventID = 42;
            let event: Promise<BoardgameEvent>;
            const expectedErrorMessage = "Some DB error";
            spyOn(GameFinder, "find"); // Prevent calling real API

            pgtest.expect("SELECT bggid, eventdate FROM boardgameevents WHERE id=" + eventID + ";").returning(new Error(expectedErrorMessage), null);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                event = instance.getEvent(eventID);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                await event;
            } catch (err) {
                expect(err.message).toEqual(expectedErrorMessage);
            } finally {
                endTest();
            }
        });
    });

    describe("registerUserToEvent tests with pgtest", () => {

        beforeEach(() => {
            pgtest.reset();
        });

        it("registerUserToEvent should resolve the promise if there is no problem", async (endTest) => {
            // given
            const teamID = "T321235";
            const eventID = 42;
            const userID = "U43233";
            let result: Promise<void>;
            const canJoin = true;

            pgtest.expect("UPDATE participations SET canjoin='" + canJoin + "' WHERE  event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'; " +
                "INSERT INTO participations (event_id, user_slack_id, team_slack_id, canjoin) SELECT " + eventID + " , '" + userID + "','" + teamID + "', '" + canJoin + "' " +
                "WHERE NOT EXISTS (SELECT 1 FROM participations WHERE event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'); ").returning(null, []);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                result = instance.registerUserToEvent(teamID, userID, eventID, canJoin);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                await result;
            } finally {
                endTest();
            }
        });
        it("registerUserToEvent should reject if DB error occurs", async (endTest) => {
            // given
            const eventID = 42;
            const teamID = "T23542";
            const userID = "U43233";
            let result: Promise<void>;
            const canJoin = true;
            const expectedErrorMessage = "Some DB error";

            pgtest.expect("UPDATE participations SET canjoin='" + canJoin + "' WHERE  event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'; " +
                "INSERT INTO participations (event_id, user_slack_id, team_slack_id, canjoin) SELECT " + eventID + " , '" + userID + "','" + teamID + "', '" + canJoin + "' " +
                "WHERE NOT EXISTS (SELECT 1 FROM participations WHERE event_id=" + eventID + " AND user_slack_id='" + userID + "' AND team_slack_id='" + teamID + "'); ").returning(
                new Error(expectedErrorMessage), null);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                result = instance.registerUserToEvent(teamID, userID, eventID, canJoin);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                await result;
            } catch (err) {
                expect(err.message).toEqual(expectedErrorMessage);
            } finally {
                endTest();
            }
        });
    });

    describe("getParticipantsForEvent tests with pgtest", () => {

        beforeEach(() => {
            pgtest.reset();
        });

        it("getParticipantsForEvent should return the participants", async (endTest) => {
            // given
            const teamID = "T4389";
            const eventID = 42;
            let result: Promise<User[]>;
            const participants = [
                new User("U1234", true),
                new User("U1235", true),
                new User("U1236", false),
                new User("U1237", true),
            ];

            pgtest.expect("SELECT user_slack_id, canjoin FROM participations WHERE event_id = " + eventID + " AND team_slack_id = '" + teamID + "';").returning(null, [
                {user_slack_id: participants[0].userId, canjoin: participants[0].canJoin},
                {user_slack_id: participants[1].userId, canjoin: participants[1].canJoin},
                {user_slack_id: participants[2].userId, canjoin: participants[2].canJoin},
                {user_slack_id: participants[3].userId, canjoin: participants[3].canJoin},
            ]);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                result = instance.getParticipantsForEvent(teamID, eventID);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                const p = await result;
                expect(p).toEqual(participants);
            } finally {
                endTest();
            }
        });

        it("getParticipantsForEvent should reject if DB error occurs", async (endTest) => {
            // given
            const teamID = "T9665";
            const eventID = 42;
            const expectedErrorMessage = "Some DB Error";

            let result: Promise<User[]>;
            pgtest.expect("SELECT user_slack_id, canjoin FROM participations WHERE event_id = " + eventID + " AND team_slack_id = '" + teamID + "';")
                    .returning(new Error(expectedErrorMessage), null);

            // when
            pgtest.connect("foo", (err, client, done) => {
                const instance: Database = new Database(client);
                client.connect = () => {
                };
                client.end = () => {
                };

                instance.connect();
                result = instance.getParticipantsForEvent(teamID, eventID);
                instance.disconnect();

                done();
            });

            // then
            try {
                pgtest.check();
                await result;
            } catch (err) {
                expect(err.message).toEqual(expectedErrorMessage);
            } finally {
                endTest();
            }
        });
    });
});
