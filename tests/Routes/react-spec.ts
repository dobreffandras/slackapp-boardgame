import {Request, Response} from "express";
import moment = require("moment");
import {ISO_8601} from "moment";
import * as TypeMoq from "typemoq";
import {BoardGame} from "../../src/boardgame";
import {BoardgameEvent} from "../../src/boardgameEvent";
import {Database} from "../../src/Database/database";
import {IClient} from "../../src/Database/iclient";
import {GameFinder} from "../../src/gamefinder";
import {BoardGameEventPollSlackMessage} from "../../src/Messages/boardgameEventPollslackMessage";
import {Messages} from "../../src/Messages/Messages";
import {ReactRoute} from "../../src/routes/react";
import {BaseRoute} from "../../src/routes/route";
import {User} from "../../src/user";

describe("React route Unit Tests", () => {

    const verifyToken = "Đevel0pMent€nvironment";

    beforeAll(() => {
        process.env.VERIFY_TOKEN = verifyToken;
    });

    it("react test user voted yes", async (endTest) => {
        // given
        const teamID = "T32534";
        const eventDBID = 1;
        const boardgame: BoardGame = {...new BoardGame(), id: eventDBID};
        const boardgameEvent = new BoardgameEvent(boardgame, moment());
        const responseURL = "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM";
        const currentUser = new User("U045VRZFT", true);
        const participants = [
            new User("U1234", true),
            new User("U1235", false),
            new User("U1236", true),
            currentUser,
        ];

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "registerUserToEvent").and.returnValue(Promise.resolve());
        spyOn(db, "getEvent").and.returnValue(Promise.resolve(boardgameEvent));
        spyOn(db, "getParticipantsForEvent").and.returnValue(Promise.resolve(participants));

        const instance: ReactRoute = new ReactRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {
                payload: JSON.stringify({
                    actions: [{
                        name: "game",
                        value: "yes",
                        type: "button",
                    }],
                    callback_id: eventDBID.toString(),
                    team: {
                        id: teamID,
                        domain: "somedomain"
                    },
                    channel: null,
                    user: {
                        id: currentUser.userId,
                        name: "brautigan",
                    },
                    action_ts: null,
                    message_ts: null,
                    attachment_id: null,
                    token: verifyToken,
                    original_message: null,
                    response_url: responseURL,
                    trigger_id: null,
                }),
            };
        });

        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendInteractiveResponse");

        try {
            // when
            await instance.react(req.object, res.object, () => {
            });

            // then
            const expectedEphemeralMessage = Messages.SuccessfulParticipationMessage;
            const expectedEventMessage = new BoardGameEventPollSlackMessage(boardgameEvent, eventDBID, participants);

            expect(db.connect).toHaveBeenCalled();
            expect(db.registerUserToEvent).toHaveBeenCalledWith(teamID, currentUser.userId, eventDBID, currentUser.canJoin);
            expect(db.getEvent).toHaveBeenCalledWith(eventDBID);
            expect(db.getParticipantsForEvent).toHaveBeenCalledWith(teamID, eventDBID);
            expect(db.disconnect).toHaveBeenCalled();
            expect(BaseRoute.sendInteractiveResponse).toHaveBeenCalledWith(req.object, res.object, expectedEventMessage.toJSON(), expectedEphemeralMessage, responseURL);

        } finally {
            endTest();
        }
    });

    it("react test user voted no", async (endTest) => {
        // given
        const teamID = "T32534";
        const eventDBID = 1;
        const boardgame: BoardGame = {...new BoardGame(), id: eventDBID};
        const boardgameEvent = new BoardgameEvent(boardgame, moment());
        const responseURL = "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM";
        const currentUser = new User("U045VRZFT", false);

        const participants = [
            new User("U1234", true),
            new User("U1235", false),
            new User("U1236", true),
            currentUser,
        ];
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "registerUserToEvent").and.returnValue(Promise.resolve());
        spyOn(db, "getEvent").and.returnValue(Promise.resolve(boardgameEvent));

        spyOn(db, "getParticipantsForEvent").and.returnValue(Promise.resolve(participants));
        const instance: ReactRoute = new ReactRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {
                payload: JSON.stringify({
                    actions: [{
                        name: "game",
                        value: "no",
                        type: "button",
                    }],
                    callback_id: eventDBID.toString(),
                    team: {
                        id: teamID,
                        domain: "somedomain"
                    },
                    channel: null,
                    user: {
                        id: currentUser.userId,
                        name: "brautigan",
                    },
                    action_ts: null,
                    message_ts: null,
                    attachment_id: null,
                    token: verifyToken,
                    original_message: null,
                    response_url: responseURL,
                    trigger_id: null,
                }),
            };
        });

        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendInteractiveResponse");

        try {
            // when
            await instance.react(req.object, res.object, () => {
            });

            // then
            const expectedEphemeralMessage = Messages.UnSuccessfulParticipationMessage;
            const expectedEventMessage = new BoardGameEventPollSlackMessage(boardgameEvent, eventDBID, participants);
            expect(db.connect).toHaveBeenCalled();
            expect(db.registerUserToEvent).toHaveBeenCalledWith(teamID, currentUser.userId, eventDBID, currentUser.canJoin);
            expect(db.getEvent).toHaveBeenCalledWith(eventDBID);
            expect(db.getParticipantsForEvent).toHaveBeenCalledWith(teamID, eventDBID);
            expect(db.disconnect).toHaveBeenCalled();
            expect(BaseRoute.sendInteractiveResponse).toHaveBeenCalledWith(req.object, res.object, expectedEventMessage.toJSON(), expectedEphemeralMessage, responseURL);
        } finally {
            endTest();
        }
    });

    it("react RegisterUser DB error", async (endTest) => {
        // given
        const teamID = "T32534";
        const eventDBID = 1;
        const DBError = "Some DB Error";
        const boardgame: BoardGame = {...new BoardGame(), id: eventDBID};
        const boardgameEvent = new BoardgameEvent(boardgame, moment());
        const responseURL = "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM";

        const currentUser = new User("U045VRZFT", false);
        const participants = [
            new User("U1234", true),
            new User("U1235", false),
            new User("U1236", true),
            currentUser,
        ];
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "registerUserToEvent").and.returnValue(Promise.reject(DBError));
        spyOn(db, "getEvent").and.returnValue(Promise.resolve(boardgameEvent));

        spyOn(db, "getParticipantsForEvent").and.returnValue(Promise.resolve(participants));
        const instance: ReactRoute = new ReactRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {
                payload: JSON.stringify({
                    actions: [{
                        name: "game",
                        value: "no",
                        type: "button",
                    }],
                    callback_id: eventDBID.toString(),
                    team: {
                        id: teamID,
                        domain: "somedomain"
                    },
                    channel: null,
                    user: {
                        id: currentUser.userId,
                        name: "brautigan",
                    },
                    action_ts: null,
                    message_ts: null,
                    attachment_id: null,
                    token: verifyToken,
                    original_message: null,
                    response_url: responseURL,
                    trigger_id: null,
                }),
            };
        });

        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendInteractiveResponse");
        spyOn(BaseRoute, "sendJSON");

        try {
            // when
            await instance.react(req.object, res.object, () => {
            });

            // then
            const expectedEphemeralMessage = Messages.UnSuccessfulParticipationMessage;
            const expectedEventMessage = new BoardGameEventPollSlackMessage(boardgameEvent, eventDBID, participants);
            expect(db.connect).toHaveBeenCalled();
            expect(db.registerUserToEvent).toHaveBeenCalledWith(teamID, currentUser.userId, eventDBID, currentUser.canJoin);
            expect(db.getEvent).not.toHaveBeenCalled();
            expect(db.getParticipantsForEvent).not.toHaveBeenCalled();
            expect(db.disconnect).toHaveBeenCalled();
            expect(BaseRoute.sendInteractiveResponse).not.toHaveBeenCalled();
            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, Messages.getEphemeralMessage(Database.DBInsertErrorMessage));
        } finally {
            endTest();
        }
    });

    it("react getEvent DB error", async (endTest) => {
        // given
        const teamID = "T32534";
        const eventDBID = 1;
        const DBError = "Some DB Error";
        const responseURL = "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM";
        const currentUser = new User("U045VRZFT", false);
        const participants = [
            new User("U1234", true),
            new User("U1235", false),
            new User("U1236", true),
            currentUser,
        ];

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "registerUserToEvent").and.returnValue(Promise.resolve());
        spyOn(db, "getEvent").and.returnValue(Promise.reject(DBError));
        spyOn(db, "getParticipantsForEvent").and.returnValue(Promise.resolve(participants));

        const instance: ReactRoute = new ReactRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {
                payload: JSON.stringify({
                    actions: [{
                        name: "game",
                        value: "no",
                        type: "button",
                    }],
                    callback_id: eventDBID.toString(),
                    team: {
                        id: teamID,
                        domain: "somedomain"
                    },
                    channel: null,
                    user: {
                        id: currentUser.userId,
                        name: "brautigan",
                    },
                    action_ts: null,
                    message_ts: null,
                    attachment_id: null,
                    token: verifyToken,
                    original_message: null,
                    response_url: responseURL,
                    trigger_id: null,
                }),
            };
        });

        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendInteractiveResponse");
        spyOn(BaseRoute, "sendJSON");

        try {
            // when
            await instance.react(req.object, res.object, () => {
            });

            // then
            expect(db.connect).toHaveBeenCalled();
            expect(db.registerUserToEvent).toHaveBeenCalledWith(teamID, currentUser.userId, eventDBID, currentUser.canJoin);
            expect(db.getEvent).toHaveBeenCalledWith(eventDBID);
            expect(db.getParticipantsForEvent).not.toHaveBeenCalled();
            expect(db.disconnect).toHaveBeenCalled();
            expect(BaseRoute.sendInteractiveResponse).not.toHaveBeenCalled();
            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, Messages.getEphemeralMessage(Database.DBInsertErrorMessage));
        } finally {
            endTest();
        }
    });

    it("react getParticipantsForEvent DB error", async (endTest) => {
        // given
        const teamID = "T32534";
        const eventDBID = 1;
        const DBError = "Some DB Error";
        const boardgame: BoardGame = {...new BoardGame(), id: eventDBID};
        const boardgameEvent = new BoardgameEvent(boardgame, moment());
        const responseURL = "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM";
        const currentUser = new User("U045VRZFT", false);

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "registerUserToEvent").and.returnValue(Promise.resolve());
        spyOn(db, "getEvent").and.returnValue(Promise.resolve(boardgameEvent));
        spyOn(db, "getParticipantsForEvent").and.returnValue(Promise.reject(DBError));

        const instance: ReactRoute = new ReactRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {
                payload: JSON.stringify({
                    actions: [{
                        name: "game",
                        value: "no",
                        type: "button",
                    }],
                    callback_id: eventDBID.toString(),
                    team: {
                        id: teamID,
                        domain: "somedomain"
                    },
                    channel: null,
                    user: {
                        id: currentUser.userId,
                        name: "brautigan",
                    },
                    action_ts: null,
                    message_ts: null,
                    attachment_id: null,
                    token: verifyToken,
                    original_message: null,
                    response_url: responseURL,
                    trigger_id: null,
                }),
            };
        });

        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendInteractiveResponse");
        spyOn(BaseRoute, "sendJSON");

        try {
            // when
            await instance.react(req.object, res.object, () => {
            });

            // then
            expect(db.connect).toHaveBeenCalled();
            expect(db.registerUserToEvent).toHaveBeenCalledWith(teamID, currentUser.userId, eventDBID, currentUser.canJoin);
            expect(db.getEvent).toHaveBeenCalledWith(eventDBID);
            expect(db.getParticipantsForEvent).toHaveBeenCalledWith(teamID, eventDBID);
            expect(db.disconnect).toHaveBeenCalled();
            expect(BaseRoute.sendInteractiveResponse).not.toHaveBeenCalled();
            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, Messages.getEphemeralMessage(Database.DBInsertErrorMessage));
        } finally {
            endTest();
        }
    });

    it("react with incorrect verifyToken", async (endTest) => {
        // given
        const teamID = "T32534";
        const eventDBID = 1;
        const responseURL = "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM";
        const currentUser = new User("U045VRZFT", true);
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);

        const instance: ReactRoute = new ReactRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        const payloadMessage = JSON.stringify({
            actions: [{
                name: "game",
                value: "yes",
                type: "button",
            }],
            callback_id: eventDBID.toString(),
            team: {
                id: teamID,
                domain: "somedomain"
            },
            channel: null,
            user: {
                id: currentUser.userId,
                name: "brautigan",
            },
            action_ts: null,
            message_ts: null,
            attachment_id: null,
            token: "IncorrectVerifyTok€n",
            original_message: null,
            response_url: responseURL,
            trigger_id: null,
        });
        req.setup((r) => r.body).returns(() => {
            return { payload: payloadMessage };
        });

        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendInteractiveResponse");

        try {
            // when
            await instance.react(req.object, res.object, () => {
            });

            // then
            res.verify((x) => x.status(403), TypeMoq.Times.once());
        } finally {
            endTest();
        }
    });

});
