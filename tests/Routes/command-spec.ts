import {Request, Response} from "express";
import {ISO_8601} from "moment";
import moment = require("moment");
import * as TypeMoq from "typemoq";
import {BoardGame} from "../../src/boardgame";
import {BoardgameEvent} from "../../src/boardgameEvent";
import {Database} from "../../src/Database/database";
import {IClient} from "../../src/Database/iclient";
import {GameFinder} from "../../src/gamefinder";
import {BoardGameEventPollSlackMessage} from "../../src/Messages/boardgameEventPollslackMessage";
import {Messages} from "../../src/Messages/Messages";
import {CommandRoute} from "../../src/routes/command";
import {BaseRoute} from "../../src/routes/route";

describe("Command route Unit Tests", () => {

    const verifyToken = "Đevel0pMent€nvironment";

    beforeAll(() => {
        process.env.VERIFY_TOKEN = verifyToken;
    });

    it("command test", async (endTest) => {
        // given
        const slashCommand = "1234 2017-11-06 13:00";
        const BGGID = 1234;
        const boardgame: BoardGame = {...new BoardGame(), id: BGGID};
        const DBInsertId = 1;

        spyOn(GameFinder, "find").and.returnValue(Promise.resolve(boardgame));

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "saveEvent").and.returnValue(Promise.resolve(DBInsertId));

        const instance: CommandRoute = new CommandRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {text: slashCommand, token: verifyToken};
        });
        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendJSON");

        try {

            // when
            await instance.command(req.object, res.object, () => {
            });

            // then
            expect(GameFinder.find).toHaveBeenCalledWith(BGGID);

            const expectedBoardgameEvent = new BoardgameEvent(boardgame, moment("2017-11-06 13:00", [ISO_8601]));
            const expectedPoll = new BoardGameEventPollSlackMessage(expectedBoardgameEvent, DBInsertId);
            expect(db.connect).toHaveBeenCalled();
            expect(db.saveEvent).toHaveBeenCalledWith(expectedBoardgameEvent);
            expect(db.disconnect).toHaveBeenCalled();

            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, expectedPoll.toJSON());

            res.verify((x) => x.setHeader(TypeMoq.It.isValue("Content-Type"), TypeMoq.It.isValue("application/json")), TypeMoq.Times.once());
        } finally {
            endTest();
        }
    });

    it("command test with DB error", async (endTest) => {
        // given
        const slashCommand = "1234 2017-11-06 13:00";
        const BGGID = 1234;
        const boardgame: BoardGame = {...new BoardGame(), id: BGGID};
        const DBError = "Some DB Error";

        spyOn(GameFinder, "find").and.returnValue(Promise.resolve(boardgame));

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "saveEvent").and.returnValue(Promise.reject(DBError));

        const instance: CommandRoute = new CommandRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {text: slashCommand, token: verifyToken};
        });
        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendJSON");

        try {

            // when
            await instance.command(req.object, res.object, () => {
            });

            // then
            expect(GameFinder.find).toHaveBeenCalledWith(BGGID);
            const expectedBoardgameEvent = new BoardgameEvent(boardgame, moment("2017-11-06 13:00", [ISO_8601]));

            expect(db.connect).toHaveBeenCalled();
            expect(db.saveEvent).toHaveBeenCalledWith(expectedBoardgameEvent);

            expect(db.disconnect).toHaveBeenCalled();
            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, Messages.getEphemeralMessage(Database.DBInsertErrorMessage));
        } finally {
            endTest();
        }
    });

    it("command test with GameFinder error", async (endTest) => {

        // given
        const slashCommand = "1234 2017-11-06 13:00";
        const BGGID = 1234;
        const gameFinderError = "Game couldn't be found";

        spyOn(GameFinder, "find").and.returnValue(Promise.reject(gameFinderError));

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "saveEvent");

        const instance: CommandRoute = new CommandRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {text: slashCommand, token: verifyToken};
        });
        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendJSON");

        try {
            // when
            await instance.command(req.object, res.object, () => {
            });

            // then
            expect(GameFinder.find).toHaveBeenCalledWith(BGGID);

            expect(db.connect).not.toHaveBeenCalled();
            expect(db.saveEvent).not.toHaveBeenCalled();
            expect(db.disconnect).not.toHaveBeenCalled();

            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, Messages.getEphemeralMessage(GameFinder.GameDoesNotExistErrorMessage));
        } finally {
            endTest();
        }
    });

    it("command test with invalid input", async (endTest) => {

        // given
        const slashCommand = "SomeInvalidInput";
        spyOn(GameFinder, "find");

        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);
        spyOn(db, "connect");
        spyOn(db, "disconnect");
        spyOn(db, "saveEvent");

        const instance: CommandRoute = new CommandRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {text: slashCommand, token: verifyToken};
        });
        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();
        spyOn(BaseRoute, "sendJSON");

        try {
            // when
            await instance.command(req.object, res.object, () => {
            });

            // then
            expect(GameFinder.find).not.toHaveBeenCalled();

            expect(db.connect).not.toHaveBeenCalled();
            expect(db.saveEvent).not.toHaveBeenCalled();
            expect(db.disconnect).not.toHaveBeenCalled();

            expect(BaseRoute.sendJSON).toHaveBeenCalledWith(req.object, res.object, jasmine.any("ITextMessage"));
        } finally {
            endTest();
        }
    });

    it("command test with incorrect verifyToken", async (endTest) => {
        // given
        const slashCommand = "1234 2017-11-06 13:00";
        const client: TypeMoq.IMock<IClient> = TypeMoq.Mock.ofType<IClient>();
        const db: Database = new Database(client.object);

        const instance: CommandRoute = new CommandRoute(db);
        const req: TypeMoq.IMock<Request> = TypeMoq.Mock.ofType<Request>();
        req.setup((r) => r.body).returns(() => {
            return {text: slashCommand, token: "1ncorr€ctVerifyT0ken"};
        });
        const res: TypeMoq.IMock<Response> = TypeMoq.Mock.ofType<Response>();

        try {
            // when
            await instance.command(req.object, res.object, () => {
            });

            // then
            res.verify((x) => x.status(403), TypeMoq.Times.once());
        } finally {
            endTest();
        }
    });
});
