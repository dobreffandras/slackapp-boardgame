import {GameFinder} from "../src/gamefinder";
import * as Parser from "xml2json"
import {BoardGame} from "../src/boardgame";

describe("GameFinder Unit Tests", () => {
    describe('find tests', () => {

        it('find should return the appropriate board game', async (endTest) => {
            // given
            const BGGID = 30549;
            const expectedBoardgame = {
                ...new BoardGame(),
                id: BGGID,
                Name: "Pandemic",
                MinPlayers: 2,
                MaxPlayers: 4,
                MinPlayingTime: 45,
                MaxPlayingTime: 45,
                ThumbnailURL: "https://cf.geekdo-images.com/images/pic1534148_t.jpg"
            };
            const body = "<boardgames termsofuse=\"http://boardgamegeek.com/xmlapi/termsofuse\">\n" +
                "\t<boardgame objectid=\"30549\">\n" +
                "\t\t<yearpublished>2008</yearpublished>\n" +
                "\t\t<minplayers>2</minplayers>\n" +
                "\t\t<maxplayers>4</maxplayers>\n" +
                "\t\t<playingtime>45</playingtime>\n" +
                "\t\t<minplaytime>45</minplaytime>\n" +
                "\t\t<maxplaytime>45</maxplaytime>\n" +
                "\t\t<age>8</age>\n" +
                "\t\t<name  sortindex=\"1\">EPIZOotic</name>\n" +
                "\t\t<name  sortindex=\"1\">Pandemia</name>\n" +
                "\t\t<name  sortindex=\"1\">Pandemia: Una Nuova Sfida</name>\n" +
                "\t\t<name primary=\"true\" sortindex=\"1\">Pandemic</name>\n" +
                "\t\t<name  sortindex=\"1\">Pandémie</name>\n" +
                "\t\t<name  sortindex=\"1\">Pandemie</name>\n" +
                "\t\t<name  sortindex=\"1\">Πανδημία</name>\n" +
                "\t\t<name  sortindex=\"1\">Пандемия</name>\n" +
                "\t\t<name  sortindex=\"1\">パンデミック</name>\n" +
                "\t\t<name  sortindex=\"1\">パンデミック：新たなる試練</name>\n" +
                "\t\t<name  sortindex=\"1\">瘟疫危機 (瘟疫危机)</name>\n" +
                "\t\t<name  sortindex=\"1\">팬데믹</name>\n" +
                "\t\t<description>In Pandemic, several virulent diseases have broken out simultaneously all over the world! The players are disease-fighting specialists whose mission is to treat disease hotspots while researching cures for each of four plagues before they get out of hand.&lt;br/&gt;&lt;br/&gt;The game board depicts several major population centers on Earth. On each turn, a player can use up to four actions to travel between cities, treat infected populaces, discover a cure, or build a research station. A deck of cards provides the players with these abilities, but sprinkled throughout this deck are Epidemic! cards that accelerate and intensify the diseases' activity. A second, separate deck of cards controls the &amp;quot;normal&amp;quot; spread of the infections.&lt;br/&gt;&lt;br/&gt;Taking a unique role within the team, players must plan their strategy to mesh with their specialists' strengths in order to conquer the diseases. For example, the Operations Expert can build research stations which are needed to find cures for the diseases and which allow for greater mobility between cities; the Scientist needs only four cards of a particular disease to cure it instead of the normal five&amp;mdash;but the diseases are spreading quickly and time is running out. If one or more diseases spreads beyond recovery or if too much time elapses, the players all lose. If they cure the four diseases, they all win!&lt;br/&gt;&lt;br/&gt;The 2013 edition of Pandemic includes two new characters&amp;mdash;the Contingency Planner and the Quarantine Specialist&amp;mdash;not available in earlier editions of the game.&lt;br/&gt;&lt;br/&gt;Pandemic is the first game in the Pandemic series.&lt;br/&gt;&lt;br/&gt;</description>\n" +
                "\t\t<thumbnail>https://cf.geekdo-images.com/images/pic1534148_t.jpg</thumbnail>\n" +
                "\t\t<image>https://cf.geekdo-images.com/images/pic1534148.jpg</image>\n" +
                "\t</boardgame>\n" +
                "</boardgames>\n";
            const jsonBody = JSON.parse(Parser.toJson(body));
            spyOn<any>(GameFinder, "call").and.returnValue(Promise.resolve(jsonBody.boardgames.boardgame));

            // when
            const result = GameFinder.find(BGGID);

            // then
            try {
                const game = await result;
                expect(game).toEqual(expectedBoardgame);
            } finally {
                endTest();
            }
        });
        it('find should reject if the item is not found', async (endTest) => {
            // given
            const BGGID = 30549;
            const body = "<boardgames termsofuse=\"http://boardgamegeek.com/xmlapi/termsofuse\">\n" +
                "\t<boardgame>\n" +
                "\t\t<error message=\"Item not found\"/>\n" +
                "\t</boardgame>\n" +
                "</boardgames>";
            const jsonBody = JSON.parse(Parser.toJson(body));
            spyOn<any>(GameFinder, "call").and.returnValue(Promise.resolve(jsonBody.boardgames.boardgame));

            // when
            const result = GameFinder.find(BGGID);

            // then
            try {
                await result;
            } catch (reason) {
                expect(reason).toEqual("Item not found");
            } finally {
                endTest();
            }
        });
        it('find should reject if there is some HTTP error', async (endTest) => {
            // given
            const BGGID = 30549;
            const expectedErrorMessage = "Some HTTP error";
            spyOn<any>(GameFinder, "call").and.returnValue(Promise.reject(expectedErrorMessage));

            // when
            const result = GameFinder.find(BGGID);

            // then
            try {
                await result;
            } catch (err) {
                expect(err).toEqual(expectedErrorMessage);
            } finally {
                endTest();
            }
        });
    });
});



