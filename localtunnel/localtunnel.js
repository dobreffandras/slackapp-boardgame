var localtunnel = require('localtunnel');
var port = 4390;
 
var tunnel = localtunnel(port, {subdomain: "boardgameslack"},function(err, tunnel) {
    if (err){
        console.log(err);
    }
 
    console.log(tunnel.url + " is up and running...")
    console.log("binded on port "+port)
});
 
tunnel.on('close', function() {
    console.log("tunnels are closed...")
});