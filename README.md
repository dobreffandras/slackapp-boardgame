# Boardgame - Slack App #

With the Boardgame app Slack members can create boardgame events, specifying the game and the time. Members can join for events.

The application uses the [BoardgameGeek API](https://boardgamegeek.com/xmlapi) for fetching some information (e.g. duration and max players) about the games for the events. 
## What is this repository for? ##

In ths repository the Node.js app is to be found which serves the Slack app.

## How to setup development environment ##

* Check out the repo
* Install Node.js and execute `npm install`
* Create a postgres database named `boardgameslack` and fill it up with the file `DatabaseSetup/db_setup.sql`.
You can either install postgres to your machine or run it in a docker container. 
	* To setup the docker environment follow the steps below:
		* Create a postgres docker container with: `docker run --name boardgame-postgres -d -p 5432:5432  -e POSTGRES_PASSWORD=postgres -v $(pwd)/DatabaseSetup/:/setupScript postgres`
		* Open a shell inside the container: `docker exec -it --user postgres --workdir /setupScript boardgame-postgres /bin/bash`
		* Create a database: type `psql` and create the database: `CREATE DATABASE boardgameslack;`
		* (Optionally you can list databases: `\l`, connect to a BD `\c <dbname>` and list tables `\d`)
		* Quit psql by `\q`
		* Setup the db: `psql -d boardgameslack -f db_setup.sql`
	* To setup postgres directly on you machine:
		* Go to `https://www.postgresql.org/` and download the latest version.
		* Install it with PgAdmin.
		* Create a database named `boardgameslack`
		* Copy the content of `db_setup.sql` in to PgAdmin Query Tool (or use `psql` as above)
* Review the environment variables in `devenv.json` to comply your database settings (The DB host may be the ip of the docker machine instead of `localhost` if you run the DB in docker).
* You should be able to run the app with some of the npm scripts found in `package.json` (eg.: `npm run-script dev`)

## Running tests ##
Tests can be run with the `npm run-script test`. The main Framework for tests is _[Jasmine](https://jasmine.github.io/)_. 
For mocking TypeScript interfaces the _[typemoq](https://github.com/florinn/typemoq)_ library is used.
The database connection mocking framework is _[pg-test](https://github.com/thepeak99/node-postgres-test)_.

## Deployment instructions ##

The application is intented to be deployed to Heroku.

### Publish on Heroku ###

For publishing the application on Heroku you need to create an account and get the heroku CLI. ([see the official instructions](https://devcenter.heroku.com/articles/getting-started-with-nodejs#introduction).)

The application can be deployed as a docker container on heroku. For that you need to build the app and build the docker image.
To build the app use the 
`grunt build-clean`
command. After that you can build and publish the container: `heroku container:push web` ([see more](https://devcenter.heroku.com/articles/container-registry-and-runtime))

The Postgres Add-on is required on Heroku to be able to run the application.

Don't forget to set up the database as well on Heroku. 

* Install the postgres plugin on Heroku.
* From the environment variables you can retrieve the connection string of the DB. ([see the structure](https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING))
* You can use the `psql` command with the options: `-h, -p, -U, -W ` and the `-f` option to execute the `DatabaseSetup/db_setup.sql` script.

For operation some environment variables need to be set up on Heroku ([How?](https://devcenter.heroku.com/articles/config-vars)):

* The `DATABASE_URL` which holds the connection string to the postgres database.
* The `CLIENT_ID` which is provided by the Slack App
* The `CLIENT_SECRET` which is provided by the Slack App
* The `VERIFY_TOKEN` which is provided by the Slack App (used to verify the request is coming from the app)

### Create Slack App ###

For creating a Slack App visit the official API [documentation](https://api.slack.com/slack-apps).

The features required are: _Interactive Components_, _Slash commands_, and Permissions

* Under _Interactive Components_ you have to specify the RequestURL which has to be `<domain_name>/react`. 
* Under _Slash Commands_ you have to add a `/boardgame` command. The belonging RequestURL is `<domain_name>/command`
* Under _Oauth & Permissions_ a Redirect URL has to be added which should be `<domain_name>/oauth`. Additionally _Add commands to <your_workspace>_ needs to be added to the _Scopes_ of the app.

## Development with Localtunel ##
Without Heroku the app can also be tested with [localtunnel](https://localtunnel.github.io/www/).
Localtunnel will bind a unique publicly accessible url to you Node.js server running locally. This URL can be then registered into the Slack app.

* Run the localtunnel instance with `node .\localtunnel\localtunnel.js`
* Run the application with `npm run-script dev` (make sure the `devenv.json` also has the appropriate variables)

The application should be able to reach through `https://boardgameslack.localtunnel.me` (The subdomain can be modified in the `localtunnel.js`)

