--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: boardgameevents; Type: TABLE; Schema: public; 
--

CREATE TABLE boardgameevents (
    id integer NOT NULL,
    bggid character varying(10) NOT NULL,
    eventdate timestamp without time zone
);

--
-- Name: boardgameevents_id_seq; Type: SEQUENCE; Schema: public; 
--

CREATE SEQUENCE boardgameevents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: boardgameevents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; 
--

ALTER SEQUENCE boardgameevents_id_seq OWNED BY boardgameevents.id;


--
-- Name: participations; Type: TABLE; Schema: public; 
--

CREATE TABLE participations (
    id integer NOT NULL,
    event_id integer NOT NULL,
    user_slack_id character varying(10) NOT NULL,
    team_slack_id character varying(10) NOT NULL,
    canjoin boolean DEFAULT true NOT NULL
);

--
-- Name: participations_id_seq; Type: SEQUENCE; Schema: public; 
--

CREATE SEQUENCE participations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: participations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; 
--

ALTER SEQUENCE participations_id_seq OWNED BY participations.id;


--
-- Name: boardgameevents id; Type: DEFAULT; Schema: public; 
--

ALTER TABLE ONLY boardgameevents ALTER COLUMN id SET DEFAULT nextval('boardgameevents_id_seq'::regclass);


--
-- Name: participations id; Type: DEFAULT; Schema: public; 
--

ALTER TABLE ONLY participations ALTER COLUMN id SET DEFAULT nextval('participations_id_seq'::regclass);

--
-- Name: boardgameevents id_constraint; Type: CONSTRAINT; Schema: public; 
--

ALTER TABLE ONLY boardgameevents
    ADD CONSTRAINT id_constraint UNIQUE (id);


--
-- Name: participations participations_id_key; Type: CONSTRAINT; Schema: public; 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_id_key UNIQUE (id);


--
-- Name: participations participations_event_id_fkey; Type: FK CONSTRAINT; Schema: public; 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_event_id_fkey FOREIGN KEY (event_id) REFERENCES boardgameevents(id);


--
-- Name: public; Type: ACL; Schema: -; 
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--