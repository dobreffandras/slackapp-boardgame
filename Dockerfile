FROM node:carbon-alpine

RUN apk add --update python && \
	apk add --update make && \
	apk add --update build-base

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --only=production

# Bundle app source
COPY bin/ bin/
COPY dist/public dist/public
COPY dist/src dist/src

EXPOSE 8080
CMD [ "node", "./bin/www" ]